# taxonomy-scripts

This is a repository of scripts for short-term and small tasks related to the taxonomy. Larger tasks don't belong in this repository.

## Task table

| Task                                         | Command                                   | Output                                                                   |
|----------------------------------------------|-------------------------------------------|--------------------------------------------------------------------------|
| Export csv table with skills and occupations | `./export_occupations_and_skills.clj run` | The file `build/export_occupations_and_skills/skills_per_occupation.csv` |

## License

Copyright © 2023 Arbetsförmedlingen JobTech

License to be decided.
