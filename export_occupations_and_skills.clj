#!/usr/bin/env bb

(ns export-occupations-and-skills
  (:require [babashka.http-client :as http-client]
            [cheshire.core :as json]
            [clojure.string :as str]
            [clojure.java.io :as io]
            [clojure.data.csv :as csv]))


#_ (-> "https://taxonomy-prod-read.api.jobtechdev.se/v1/taxonomy/graphql?query=query%20MyQuery%20%7B%0A%20%20concepts(type%3A%20%22skill%22%2C%20limit%3A%20100)%20%7B%0A%20%20%20%20preferred_label%0A%20%20%20%20id%0A%20%20%20%20related%20%7B%0A%20%20%20%20%20%20type%0A%20%20%20%20%7D%0A%20%20%7D%0A%7D%0A" (http-client/get {:headers {"Accept" "application/json"}}) :body)

(def default-config {:graphql-url "https://taxonomy-prod-read.api.jobtechdev.se/v1/taxonomy/graphql"
                     :work-dir (io/file "build/export_occupations_and_skills")})

(def query-skill-related "query MyQuery {
  concepts(type: \"skill\", version: %d) {
    preferred_label
    alternative_labels
    id
    related {
      id
    }
    replaces {
      id
    }
    substitutes {
      id
    }
  }
}")

(def query-occupations-and-ssyk "query MyQuery {
  concepts(type: \"occupation-name\") {
    preferred_label
    id
    type
    broader(type: \"ssyk-level-4\") {
      id
      type
      preferred_label
      broader(type: \"ssyk-level-3\") {
        id
        type
        preferred_label
        broader(type: \"ssyk-level-2\") {
          id
          type
          preferred_label
          broader(type: \"ssyk-level-1\") {
            id
            type
            preferred_label
          }
        }
      }
    }
  }
}
")

(def query-versions "query MyQuery { versions { id } }")


(defn retry-fn [f n]
  (loop [exceptions []]
    (if (= (count exceptions) n)
      (throw (first exceptions))
      (let [[result ex] (try
                          [(f) nil]
                          (catch Exception e
                            [nil e]))]
        (if ex
          (do (println "Retry")
              (recur (conj exceptions ex)))
          result)))))

(defmacro retry [expr n]
  `(retry-fn (fn [] ~expr) ~n))

(defn query-graphql [url query]
  (retry (->> query
              (assoc-in {:headers {"Accept" "application/json"}} [:query-params "query"])
              (http-client/get url)
              :body
              (#(json/parse-string % true)))
         3))

(defn cached-query-graphql [{:keys [graphql-url work-dir]} query-name query]
  (let [cache-file (io/file work-dir (str query-name ".json"))]
    (if (.exists cache-file)
      (-> cache-file
          slurp
          (json/parse-string true))
      (do (println "Perform" query-name)
          (let [data (query-graphql graphql-url query)]
            (io/make-parents cache-file)
            (->> data
                 json/generate-string
                 (spit cache-file))
            data)))))

(defn unwrap-graphql [x]
  (-> x :data :concepts))

(defn all-broader-ids [concept]
  (->> concept
       (iterate (comp first :broader))
       (into #{} (comp (take-while some?) (map :id)))))

(defn make-related-skill-map [skills]
  (reduce (fn [dst [related-id skill-id]]
            (update dst related-id #(conj (or % []) skill-id)))
          {}
          (for [skill skills
                related (:related skill)]
            [(:id related) (:id skill)])))

(defn broader-seq [concept]
  (->> [concept]
       (iterate #(mapcat :broader %))
       (take-while seq)
       (into [] (comp cat (map #(dissoc % :broader))))))

(def types-to-export [:skill :occupation-name :ssyk-level-4 :ssyk-level-3 :ssyk-level-2 :ssyk-level-1])

(defn fields-for-type [t]
  (into [[:id "id"]
         [:preferred_label "preferred-label"]]
        (if (= t :skill)
          [[:version "version"]]
          [])))

(defn make-row [data]
  (for [k types-to-export
        [k2 _] (fields-for-type k)]
    (get-in data [k k2])))

(defn make-header []
  (for [k types-to-export
        [_ k2] (fields-for-type k)]
    (str (name k) "/" k2)))

(defn occupation-sort-order [occupation]
  (vec (for [t (reverse types-to-export)]
         (-> occupation t :preferred_label))))

(defn get-versions [config]
  (->> query-versions
       (cached-query-graphql config "versions")
       :data
       :versions
       (map :id)))

(defn get-skills-for-all-versions [config versions]
  (for [version versions
        concept (unwrap-graphql
                 (cached-query-graphql
                  config
                  (format "skill-related_version%d" version)
                  (format query-skill-related version)))]
    (assoc concept :version version)))

(defn labels-per-skill [skills]
  (into {} (for [[id group] (group-by :id skills)]
             [id (reduce (fn [dst [k version]]
                           (update dst k #(conj (or % #{}) version)))
                         {}
                         (for [skill group
                               [label-type labels] [[:preferred_label [(:preferred_label skill)]]
                                                    [:alternative_labels (:alternative_labels skill)]]
                               label labels]
                           [{:label label
                             :type label-type}
                            (:version skill)]))])))

(defn skill-label-table [lps]
  (into [["Id" "Label" "Label type" "Versions"]]
        (for [[id labels] lps
              [{:keys [label type]} versions] labels]
          [id label (name type) (str/join ", " (sort versions))])))

(defn skills-per-version [all-skills]
    (reduce (fn [dst skill]
              (assoc-in dst [(:version skill) (:id skill)] skill))
            {}
            all-skills))

(defn invert-replaces [spv]
  (let [all-versions (-> spv keys sort reverse)]
    (reduce (fn [spv [version old-id new-id]]
              (if-let [old-skill (first (for [v all-versions
                                              old-skill [(get-in spv [v old-id])]
                                              :when old-skill]
                                          old-skill))]
                (let [skill-to-insert (assoc old-skill
                                             :version version
                                             :replaced_by [{:id new-id}])]
                  (assoc-in spv
                            [version old-id]
                            skill-to-insert))
                (do (println "Not found:" old-id)
                    spv)))
            spv
            (for [[version skill-map] spv
                  [new-id skill] skill-map
                  old (:replaces skill)]
              [version (:id old) new-id]))))

(defn skill-history [spv version id]
  (lazy-seq
   (when-let [vmap (spv version)]
     (if-let [concept (vmap id)]
       (cons concept
             (if-let [[replacement & r] (seq (:replaced_by concept))]
               (do (assert (nil? r))
                   (println "Replacement" replacement)
                   (skill-history spv version (:id replacement)))
               (skill-history spv (inc version) id)))
       (skill-history spv (inc version) id)))))

(defn full-skill-history [spv id]
  (skill-history spv (apply min (keys spv)) id))

(defn label-to-skill-id-map [skill-label-map]
  (reduce (fn [dst [label skill-id versions]]
            (assoc-in dst [label skill-id] versions))
          {}
          (for [[skill-id label-version-map] skill-label-map
                [{:keys [label]} versions] label-version-map]
            [label skill-id versions])))

(defn split-by [s sep]
  (->> (str/split s sep)
       (map str/trim)
       (filter seq)))

(defn finding-matches [skill-label-map]
  (let [label-skill-map (label-to-skill-id-map skill-label-map)]
    (comp (mapcat (fn [urk]
                    (let [label (get urk "URK-begrepp")
                          cands (label-skill-map label)]
                      (if cands
                        [(assoc urk
                                :cands cands
                                :type :exact
                                :matching-label label)]
                        (or (seq (for [group [(split-by (:Alternativ urk "") #":")]
                                       term group
                                       [k cands] label-skill-map
                                       :when (str/includes? k term)]
                                   (do (println "MJAO")
                                       (assoc urk
                                              :cands cands
                                              :type :approx
                                              :matching-label k
                                              :term term))))
                            [(assoc urk :type :not-found)])))))
          (mapcat (fn [{:keys [type cands] :as item}]
                    (case type
                      :not-found [item]
                      (for [[skill-id _] cands]
                        (-> item
                            (dissoc :cands)
                            (assoc :skill-id skill-id)))))))))

(defn take-first
  ([] nil)
  ([dst] dst)
  ([_dst x] (reduced x)))

(defn perform-urk-mapping [version-skill-map
                           skill-label-map
                           in-file out-file]
  (let [versions-of-interest #{16}
        [header-row & body] (with-open [r (io/reader in-file)]
                              (vec (csv/read-csv r)))
        data (for [row body] (zipmap header-row row))
        results (sort-by (fn [{:keys [type skill-id skill-of-interest]}]
                           [(case type
                              :not-found 0
                              :approx 1
                              :exact 2)
                            (cond
                              (nil? skill-of-interest) 0
                              (not= (:id skill-of-interest) skill-id) 1
                              :else 2)])
                         (into []
                               (comp (finding-matches skill-label-map)
                                     (map (comp (fn [{:keys [skill-id] :as item}]
                                                  (let [skill-of-interest (first
                                                                           (filter
                                                                            (comp versions-of-interest :version)
                                                                            (full-skill-history version-skill-map skill-id)))]
                                                    
                                                    (assoc item :skill-of-interest skill-of-interest))))))
                               data))
        rows (into [["URK-begrepp" "Matchande id" "Matchande etikett" "Id version 16" "Begrepp version 16" "Ersätter?"]]
                   (for [{:keys [skill-id skill-of-interest] :as urk} results]
                     [(urk "URK-begrepp")
                      (urk :skill-id "")
                      (urk :matching-label "")
                      (get skill-of-interest :id)
                      (get skill-of-interest :preferred_label)
                      (if (and skill-of-interest (not= (:id skill-of-interest) skill-id))
                        "JA"
                        "")]))]
    (with-open [w (io/writer out-file)]
      (csv/write-csv w rows))
    results))

(defn run [config]
  (let [versions (get-versions config)
        work-dir (:work-dir config)
        all-skills (get-skills-for-all-versions config versions)
        skill-map (into {} (map (juxt :id identity)) all-skills)
        skills (vals skill-map)
        version-skill-map (-> all-skills skills-per-version invert-replaces)
        occupations (unwrap-graphql (cached-query-graphql config "occupations-and-ssyk" query-occupations-and-ssyk))
        related-skill-map (make-related-skill-map skills)
        occupations-with-skills (for [occupation occupations]
                                  (let [related-skills (for [broader-id (all-broader-ids occupation)
                                                             rel (-> broader-id related-skill-map)]
                                                         (-> rel skill-map (dissoc :related)))]
                                    (->> occupation
                                         broader-seq
                                         (into {:skills (or (seq related-skills) [{}])}
                                               (map (juxt (comp keyword :type) identity))))))
        occupation-skill-pairs  (for [occupation (sort-by occupation-sort-order occupations-with-skills)
                                      skill (:skills occupation)]
                                  (-> occupation
                                      (dissoc :skills)
                                      (assoc :skill skill)
                                      make-row))
        rows (into [(make-header)] occupation-skill-pairs)
        skill-label-map (labels-per-skill all-skills)
        out-file (io/file work-dir "skills_per_occupation.csv")
        label-file (io/file work-dir "skill_labels.csv")]
    (io/make-parents out-file)
    (perform-urk-mapping
     version-skill-map
     skill-label-map
     (io/file "resources/127_URK-begrepp_som_saknas_i_taxonomin.csv")
     (io/file work-dir "urk_mapping.csv"))
    (with-open [w (io/writer out-file)]
      (csv/write-csv w rows))
    (with-open [w (io/writer label-file)]
      (csv/write-csv w (skill-label-table skill-label-map)))))

(defn -main [& args]
  (case (first args)
    "run" (run default-config)
    nil nil))

(apply -main *command-line-args*)
